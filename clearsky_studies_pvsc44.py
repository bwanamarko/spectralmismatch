# -*- coding: utf-8 -*-
"""
Linke turbidity and clear sky analysis for PVSC-44

Created on Wed Jan 25 23:09:51 2017
(c) 2017 SunPower
@author: mmikofski
"""

import os
import sys
import logging

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import pytz

import pvlib
from utils import get_nrel_data
from spectral_correction import Atmosphere

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

# make sure we are using the local version
PVLIB_PATH = os.path.abspath(pvlib.__file__)
if 'Python27' in PVLIB_PATH:
    LOGGER.error('pvlib: %s', PVLIB_PATH)
    sys.exit(-1)

# NREL mPERT dataset
SITE = 'Cocoa'
PVMOD_ID = 'xSi12922'
CELLTECH = 'cSi'
FILENAME = os.path.join(SITE, '%s_%s.csv' % (SITE, PVMOD_ID))
HEADER, DATA = get_nrel_data(FILENAME)
TZ = pytz.timezone('Etc/GMT+5')
MPERT = pd.DataFrame(
    {'dni': DATA['dni'], 'ghi': DATA['ghi'], 'dhi': DATA['dhi'],
     'dry_temp': DATA['dry_temp'], 'rh': DATA['rh'],
     'patm': DATA['patm'] * 100.0, 'timestamp': DATA['timestamp']}, 
    index=pd.DatetimeIndex(DATA['timestamp']).tz_localize(TZ)
)
# drop duplicated indices and missing data marked -9999
_DUPLICATES = MPERT.index.duplicated()
if _DUPLICATES.any():
    LOGGER.warning('duplicate indices:\n%s', MPERT[_DUPLICATES])
    MPERT = MPERT.drop_duplicates(subset='timestamp')
for colname in MPERT.columns:
    if colname == 'timestamp':
        continue
    MPERT = MPERT.where(MPERT[colname] != -9999.0, np.nan).dropna()

# solarposition, airmass, pressure-corrected & water vapor
SP = pvlib.solarposition.get_solarposition(
    time=MPERT.index,
    latitude=HEADER['lat'], longitude=HEADER['lon'], altitude=HEADER['alt'],
    pressure=MPERT['patm'], temperature=MPERT['dry_temp']
)
ZENITH_REFRACTED = SP.apparent_zenith
AM = pvlib.atmosphere.relativeairmass(ZENITH_REFRACTED)
AMP = pvlib.atmosphere.absoluteairmass(AM, pressure=MPERT['patm'])
PWAT = pvlib.atmosphere.gueymard94_pw(MPERT['dry_temp'], MPERT['rh'])

# ECMWF data for 2011-2012
ATMOS = Atmosphere()
ATM_DATA = pd.DataFrame([
    ATMOS.get_data(
        HEADER['lat'], HEADER['lon'], dt.to_pydatetime()
    ) for dt in MPERT.index.tz_convert(None)
], index=MPERT.index)

# lookup Linke turbidity
LT = pvlib.clearsky.lookup_linke_turbidity(
    time=MPERT.index,
    latitude=HEADER['lat'],
    longitude=HEADER['lon'],
)
LT_CALC = pvlib.atmosphere.kasten96_lt(
    ATM_DATA['tau500'], AMP, ATM_DATA['pwat'], ATM_DATA['alpha']
)

# calculate clear sky
ETR = pvlib.irradiance.extraradiation(MPERT.index)
INEICHEN_LT = pvlib.clearsky.ineichen(
    ZENITH_REFRACTED, AMP, LT, altitude=HEADER['alt'], dni_extra=ETR
)
INEICHEN_CALC = pvlib.clearsky.ineichen(
    ZENITH_REFRACTED, AMP, LT_CALC, altitude=HEADER['alt'], dni_extra=ETR
)
SOLIS = pvlib.clearsky.simplified_solis(
    SP.apparent_elevation, ATM_DATA['tau700'], ATM_DATA['pwat'],
    pressure=MPERT['patm'], dni_extra=ETR
)
AOD380 = pvlib.atmosphere.angstrom_aod_at_lambda(
    [(550.0, ATM_DATA['aod550'])], ATM_DATA['alpha'], lambda0=380.0
)
BIRD = pd.DataFrame(dict(zip(
    ['dni','beam_hz','ghi','dhi'],
    pvlib.clearsky.bird(
        ZENITH_REFRACTED, AM, AOD380, ATM_DATA['tau500'], ATM_DATA['pwat'],
        pressure=MPERT['patm'], dni_extra=ETR
    )
)), index=MPERT.index)

# %matplotlib qt
pd.concat(
    [LT.resample('M').mean(), LT_CALC.resample('M').mean()], axis=1
).tz_convert('US/Eastern').plot()
plt.legend(['Linke', 'Kasten'])
plt.title(
    'Historical Linke turbidity factors compared to 2011-2012 calculations'
)
plt.ylabel('Linke turbidity factor')
plt.grid()

pd.concat(
    [PWAT.resample('M').mean(), ATM_DATA['pwat'].resample('M').mean()], axis=1
).tz_convert('US/Eastern').plot()
plt.legend(['Gueymard', 'MACC'])
plt.title('Precipitable Water comparison')
plt.ylabel('$P_{wat} [cm]$')
plt.grid()

LEGEND = ['Ineichen w/Linke', 'Ineichen w/MACC', 'Solis', 'Bird', 'mPERT']
pd.concat([
    INEICHEN_LT['ghi'].resample('M').mean(),
    INEICHEN_CALC['ghi'].resample('M').mean(),
    SOLIS['ghi'].resample('M').mean(),
    BIRD['ghi'].resample('M').mean(),
    MPERT['ghi'].resample('M').mean()
], axis=1).tz_convert('US/Eastern').plot()
plt.legend(LEGEND)
plt.title('Comparison of clear sky irradiance at Cocoa-FL, 2011-2012')
plt.ylabel('$GHI [W/m^2]$')
plt.grid()

pd.concat([
    INEICHEN_LT['dhi'].resample('M').mean(),
    INEICHEN_CALC['dhi'].resample('M').mean(),
    SOLIS['dhi'].resample('M').mean(),
    BIRD['dhi'].resample('M').mean(),
    MPERT['dhi'].resample('M').mean()
], axis=1).tz_convert('US/Eastern').plot()
plt.legend(LEGEND)
plt.title('Comparison clear sky irradiance at Cocoa-FL, 2011-2012')
plt.ylabel('$DHI [W/m^2]$')
plt.grid()

pd.concat([
    INEICHEN_LT['dni'].resample('M').mean(),
    INEICHEN_CALC['dni'].resample('M').mean(),
    SOLIS['dni'].resample('M').mean(),
    BIRD['dni'].resample('M').mean(),
    MPERT['dni'].resample('M').mean()
], axis=1).tz_convert('US/Eastern').plot()
plt.legend(LEGEND)
plt.title('Comparison of clear sky irradiance at Cocoa-FL, 2011-2012')
plt.ylabel('$DNI [W/m^2]$')
plt.grid()
