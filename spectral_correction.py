# -*- coding: utf-8 -*-
"""
Spectral corrections for modules in NREL Report 61610 by Bill Marion


Running this module from the command line tests the spectral shift calculation
of monocrystalline silicon modules at Cocoa and creates a scatter plot of the
measured vs predicted spectral shift using QE and SPECTRL2 predicted spectra.
This module contains the several functions and classes for the calculation of
spectral shift.

Atmosphere
----------
The atmosphere class loads ECMWF aerosol optical depth and water vapor data
from netCDF4 files. The path to the folder containing the files is the only
argument, but the files must be named as follows:

    * aod550_2011_macc.nc - AOD at 550[nm] for the globe in 2011
    * aod1240_2011_macc.nc - AOD at 1240[nm] for the globe in 2011
    * tcwv_2011_macc.nc - water vapor for the globe in 2011
    * aod550_2012_macc.nc - AOD at 550[nm] for the globe in 2012
    * aod1240_2012_macc.nc - AOD at 1240[nm] for the globe in 2012
    * tcwv_2012_macc.nc - water vapor for the globe in 2012

The atmosphere class, once initialized, can be used to supply AOD and water
vapor (pwat) at any latitude, longitude coordinates at any time of the year by
calling it's ``get_data(lat, lon, dt)` method.

Spectrum Correction
-------------------
The ``correct_spectrum()`` is the workhorse of the spectral analysis. It is
called by the ``spectral_analysis.py`` module. Given the site name, module
id and celltech, it does the following:

    * loads the specified NREL mPERT dataset,
    * loads the quantum efficiency (QE) of the specified module
    * gets the standard ASTM-G173 airmass-1.5 spectrum
    * creates a fixed tilt tracker oriented according to the mPERT dataset
    * calculates solar position at each timestep
    * calculates solar angles relative to the tracker normal each time
    * gets AOD and pwat from ECMWF data at each timestep
    * calculates the solar spectrum using SPECTRL2 at each timestep
    * calculates the measured spectral shift, predicted shift using QE and
      SPECTRL2 predicted spectra and the Sandia 5th order airmass polynomial
    * Sandia angle of incidence (AOI) factor
    * effective irradiance - not considering diffuse transposition
    * cell temperater
    * predicted power using SAPM with predicted spectral shift

The output is a pandas data frame with many useful calculated and measured
parameters.

SunPower (c) 2016
"""

import os
import logging
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import netCDF4
from tracker_utils import FixedTilt
from spectral_shift import (
    get_qe_norm, get_am15, get_solpos, get_spectrum, calc_spectral_shift
)
from utils import get_nrel_data, read_sandia_model_coeffs, calc_sandia

plt.ion()
logging.basicConfig()

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
DIRNAME = os.path.dirname(__file__)
ECMWF_PATH = os.path.join(DIRNAME, 'ecmwf-macc', 'data')
SAPM = read_sandia_model_coeffs()
MISS = -9999

def wikipedia_interp2d(x, y, x1, dx, y1, dy, f):
    """
    https://en.wikipedia.org/wiki/Bilinear_interpolation
    """
    x2, y2 = x1 + dx, y1 + dy
    return np.dot(np.dot([x2 - x, x - x1], f), [[y2 - y], [y - y1]]) / dx / dy


class Atmosphere(object):
    """
    Atmospheric data from ECMWF MACC Reanalysis.
    """
    dlat, dlon, dtime = -0.75, 0.75, 3

    def __init__(self, ecmwf_path=ECMWF_PATH):
        self.aod550_2011 = netCDF4.Dataset(
            os.path.join(ecmwf_path, 'aod550_2011_macc.nc')
        )
        self.aod1240_2011 = netCDF4.Dataset(
            os.path.join(ecmwf_path, 'aod1240_2011_macc.nc')
        )
        self.tcwv_2011 = netCDF4.Dataset(
            os.path.join(ecmwf_path, 'tcwv_2011_macc.nc')
        )
        self.aod550_2012 = netCDF4.Dataset(
            os.path.join(ecmwf_path, 'aod550_2012_macc.nc')
        )
        self.aod1240_2012 = netCDF4.Dataset(
            os.path.join(ecmwf_path, 'aod1240_2012_macc.nc')
        )
        self.tcwv_2012 = netCDF4.Dataset(
            os.path.join(ecmwf_path, 'tcwv_2012_macc.nc')
        )

    @classmethod
    def interp_data(cls, lat, lon, dt, data, name):
        """
        Interpolate data using wikipedia bilinar method.
        """
        nctime = data['time']  # time
        # XXX: do NOT use round(), int() always rounds down
        ilat = int((lat - 90.0) / cls.dlat)  # lower bound
        # avoid out of bounds latitudes
        if ilat < 0:
            ilat = 0  # if lat == 90, north pole
        elif ilat > 241:
            ilat = 241  # if lat == -90, south pole
        lon = lon % 360.0  # adjust longitude from -180/180 to 0/360
        ilon = int(lon / cls.dlon) % 480  # lower bound
#        x1 = data['latitude'][ilat]
#        y1 = data['longitude'][ilon]
        # bilinear interpolation before
        before = netCDF4.date2index(dt, nctime, select='before')
        fbefore = data[name][before, ilat, ilon]
#        f12 = data[name][before, ilat, (ilon + 1) % 480]
#        f21 = data[name][before, ilat + 1, ilon]
#        f22 = data[name][before, ilat + 1, (ilon + 1) % 480]
#        f = [[f11, f12], [f21, f22]]
#        fbefore = wikipedia_interp2d(lat, lon, x1, cls.dlat, y1, cls.dlon, f)
        # bilinear interpolation after
        fafter = data[name][before + 1, ilat, ilon]
#        f12 = data[name][before + 1, ilat, (ilon + 1) % 480]
#        f21 = data[name][before + 1, ilat + 1, ilon]
#        f22 = data[name][before + 1, ilat + 1, (ilon + 1) % 480]
#        f = [[f11, f12], [f21, f22]]
#        fafter = wikipedia_interp2d(lat, lon, x1, cls.dlat, y1, cls.dlon, f)
        dt_num = netCDF4.date2num(dt, nctime.units)
        time_ratio = (dt_num - nctime[before]) / cls.dtime
        return fbefore + (fafter - fbefore) * time_ratio

    @staticmethod
    def to_wvl(wvl, aod550, alpha=1.14):
        """
        Convert AOD from tau550 to specified wavelength.
        """
        return aod550 * ((wvl / 550.0) ** (-alpha))

    def get_data(self, lat, lon, dt):
        keys = ['aod550', 'aod1240', 'tcwv']
        data = dict.fromkeys(keys)
        for v in keys:
            if dt.year in [2011, 2013]:
                yr = '2011'
                dt = dt.replace(year=2011)
            else:
                yr = '2012'
                dt = dt.replace(year=2012)
            key = '%s_%s' % (v, yr)
            data[v] = self.interp_data(lat, lon, dt, getattr(self, key), v)
        data['pwat'] = data['tcwv'] / 10.0  # convert  kg / m^2 to cm
        data['alpha'] = (
            -np.log(data['aod1240'] / data['aod550']) / np.log(1240.0 / 550.0)
        )
        data['tau500'] = self.to_wvl(500.0, data['aod550'], data['alpha'])
        data['tau700'] = self.to_wvl(700.0, data['aod550'], data['alpha'])
        return data


ATMOS = Atmosphere()

def correct_spectrum(site='Cocoa', pvmod_id='xSi12922', celltech='cSi'):
    """
    Calculate the spectral shift (correction) due to spectral mismatch.

    :param site: name of NREL mPERT site
    :type site: str
    :param pvmod_id: name of module from NREL mPERT dataset
    :type pvmod_id: str
    :param celltech: name of cell technology from NREL mPERT dataset
    :type celltech: str
    :returns: spectral shift
    """
    filename=os.path.join(site, '%s_%s.csv' % (site, pvmod_id))
    sapm = SAPM.loc[pvmod_id]  # get sandia coefficients for module from table
    header, data = get_nrel_data(filename)  # get NREL mPERT data from file
    lat, lon = header['lat'], header['lon']
    location = [lat, lon, header['tz']]
    qe, _ = get_qe_norm(plotme=False)  # get normalized quantum efficiency
    am15, e_am15, _ = get_am15(plotme=False)  # get ASTM G173 for air mass 1.5
#    may_be_missing = list(data.dtype.names[20:37])
    # Create a fixed tilt tracker for the NREL mPERT dataset
    mpert = FixedTilt(header['aspect'], header['tilt'])
    # Initialize an empty DataFrame for the calculated spectral shift output
    spectral_shift = pd.DataFrame(
        {'m': np.nan, 'f1_calc': np.nan, 'f1_meas': np.nan,'amp': np.nan,
        'tau500': np.nan, 'pwat': np.nan, 'alpha': np.nan, 'dni': data['dni'],
        'ghi': data['ghi'], 'kt': 1 - data['dhi'] / data['ghi'],'aoi': np.nan,
        'pmp_sapm': np.nan, 'pmp_calc': np.nan, 'pmp_meas': data['pmp'],
        'ipoa': data['ipoa']},
        index=pd.DatetimeIndex(data['timestamp'])
    )
    # check for duplicated indices
    duplicates = spectral_shift.index.duplicated()
    if duplicates.any():
        LOGGER.warning('duplicate indices:\n%s', spectral_shift[duplicates])
        spectral_shift['timestamp'] = data['timestamp']
        spectral_shift = spectral_shift.drop_duplicates(subset='timestamp')
        spectral_shift = spectral_shift.drop('timestamp', 1)
    # loop over data
    for d in data:
        # check for missing weather data
        if -9999 in [d['patm'], d['dry_temp']]:
            LOGGER.warning('missing data at datetime: %s', d['timestamp'])
            LOGGER.warning('patm: %g, Tdry: %g', d['patm'], d['dry_temp'])
            continue
#        if np.any(np.asarray(d)[may_be_missing].view((float,17)) == MISS):
#            pass
        dt = d['timestamp'].item()
        weather = [d['patm'], d['dry_temp']]
        angles, airmass = get_solpos(dt, location, weather)
        mpert.rotate(angles.values())
        # turbidity alpha exponent, asymmetry factor, ozone conc., AOD and
        # water vapor
        atm_data = ATMOS.get_data(lat, lon, dt)
        spec, e_spec, _ = get_spectrum(
            dt, location, weather,
            orientation=[header['tilt'],header['aspect']],
            atmos_cond=[
                atm_data['alpha'], 0.65, 0.34, atm_data['tau500'],
                atm_data['pwat']
            ],
            plotme=False
        )
        # calculate spectral shift
        spectral_shift.loc[dt]['m'] = calc_spectral_shift(
            qe[celltech], spec, am15, e_spec, e_am15
        )
        # calculate Sandia air mass function
        spectral_shift.loc[dt]['f1_calc'] = np.polyval(
            [sapm['A4'], sapm['A3'], sapm['A2'], sapm['A1'], sapm['A0']],
            airmass['am']
        )
        # calculate Sandia angle of incidence function
        f2 = np.polyval(
            [sapm['B5'], sapm['B4'], sapm['B3'], sapm['B2'], sapm['B1'],
             sapm['B0']],
            mpert.aoi.item()
        )
        # effective irradiane with no spectral mismatch
        eff_irrad_calc = (
            f2 * d['dni'] * np.cos(np.deg2rad(mpert.aoi.item())) + d['dhi']
        ) / 1000.0
        eff_irrad = d['ipoa'] / 1000.0
        cell_temp = d['mod_temp'] + eff_irrad * sapm['d(Tc)']
        spectral_shift.loc[dt]['f1_meas'] = (
            d['isc'] / sapm['Isco'] / (1.0 + sapm['aIsc'] * (cell_temp - 25.0))
            / eff_irrad / d['soiling']
        )
        spectral_shift.loc[dt]['pmp_calc'], imp_calc, vmp_calc = calc_sandia(
            sapm, cell_temp, eff_irrad * spectral_shift.loc[dt]['m']
        )
        spectral_shift.loc[dt]['pmp_sapm'], imp_sapm, vmp_sapm = calc_sandia(
            sapm, cell_temp, eff_irrad * spectral_shift.loc[dt]['f1_calc']
        )
        spectral_shift.loc[dt]['pmp_meas'] = d['pmp']
        spectral_shift.loc[dt]['amp'] = airmass['amp']
        spectral_shift.loc[dt]['tau500'] = atm_data['tau500']
        spectral_shift.loc[dt]['pwat'] = atm_data['pwat']
        spectral_shift.loc[dt]['alpha'] = atm_data['alpha']
        spectral_shift.loc[dt]['aoi'] = mpert.aoi.item()
        # show log info once a week        
        if not dt.day % 7:
            LOGGER.info(
                'dt: %s, m: %g, f1_calc: %g, f1_meas: %g, am: %g,' +
                ' tau500: %g, pwat: %g, alpha: %g, Ee: %g, Tcell: %g' +
                ' soil: %g, Ipoa: %g, aoi: %g,' +
                ' pmp_sapm: %g, pmp_calc: %g, pmp_meas: %g', dt,
                spectral_shift.loc[dt]['m'], spectral_shift.loc[dt]['f1_calc'],
                spectral_shift.loc[dt]['f1_meas'], airmass['am'],
                atm_data['tau500'], atm_data['pwat'], atm_data['alpha'],
                eff_irrad_calc, cell_temp, 1 - d['soiling'], d['ipoa'],
                mpert.aoi.item(),
                spectral_shift.loc[dt]['pmp_sapm'],
                spectral_shift.loc[dt]['pmp_calc'], d['pmp']
            )
    return spectral_shift


if __name__ == '__main__':
    LOGGER.setLevel(logging.INFO)
    logging.getLogger("utils").setLevel(logging.WARNING)
    logging.getLogger("spectral_shift").setLevel(logging.WARNING)
    logging.getLogger("tracker_utils").setLevel(logging.WARNING)
    spectral_shift = correct_spectrum()
    # filters
    filter = (
        (spectral_shift['amp'] <= 4) &
        (spectral_shift['kt'] >= 0.5) &
        (spectral_shift['ipoa'] >= 500) &
        (spectral_shift['aoi']<=45)
    )
    spectral_shift_filter = spectral_shift.dropna().loc[filter]
    # plot
    plt.scatter(x=spectral_shift_filter['f1_meas'],
                y=spectral_shift_filter['m'],
                s=spectral_shift_filter['amp'] * 10,
                c=spectral_shift_filter['pwat'] * 10,
    )