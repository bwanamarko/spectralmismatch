# -*- coding: utf-8 -*-
"""
Spectral analysis of NREL mPERT data

Running this file from the command line analyzes just the crystaline silicon
celltech modules at all three mPERT sites by calculating the measured spectral
shift, filtering for diffuse ratio, plana of array irradiance and angle of
incidence and calculating the 4 comparisons.

1. Sandia 5th order polynomial of airmass
2. Spectral shift calculated from EQE and NREL SPECTRL2 predicted spectra
3. First Solar 2-parameter model
4. 3-parameter model extending First Solar to include AOD at 500[nm]

A plot with all four comparison are overlaid with a linear regression and the
R value of the regression is given in the subplot title.

There are four tests in the module that correspond to cell technology in the
NREL mPERT datasets.

* 0: cSi, mono crystalline silicon
* 1: mSi, poly or multicrystalline silicon, mfg #1
* 2: mSi, poly or multicrystalline silicon, mfg #2
* 3: CdTe, cadmium telluride

To run other tests, you can run this interactively in a Python shell.::

    >>> from spectral_mismatch import spectral_analysis
    >>> mytest = spectral_analysis.TEST1  # select desired test from module
    >>> m, mraw = spectral_analysis.analyze_spectral_shift(mytest)
    >>> mall = pd.concat(m.values())
    >>> f = make_plots(m=mall)

(c) 2016 SunPower Corp.
"""

import logging
from spectral_correction import correct_spectrum
import numpy as np
import pandas as pd
from scipy.stats import linregress
from scipy.linalg import lstsq
from matplotlib import pyplot as plt

plt.ion()
logging.basicConfig()
logging.getLogger("utils").setLevel(logging.WARNING)
logging.getLogger("spectral_shift").setLevel(logging.WARNING)
logging.getLogger("tracker_utils").setLevel(logging.WARNING)

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
TEST0 =[('Cocoa', 'xSi12922', 'cSi'),
        ('Eugene', 'xSi12922', 'cSi'),
        ('Golden', 'xSi11246', 'cSi')]
TEST1 = [('Cocoa', 'mSi0166', 'mSi'),
         ('Eugene', 'mSi0166', 'mSi'),
         ('Golden', 'mSi0247', 'mSi')]
TEST2 = [('Cocoa', 'mSi0188', 'mSi'),
         ('Eugene', 'mSi0188', 'mSi'),
         ('Golden', 'mSi0251', 'mSi')]
TEST3 = [('Cocoa', 'CdTe75638', 'CdTe'),
         ('Eugene', 'CdTe75638', 'CdTe'),
         ('Golden', 'CdTe75669', 'CdTe')]


def analyze_spectral_shift(tests=TEST0):
    """
    Analyze spectral shift of test data and appliy clearsky filter.
    
    :param tests: list of tuples containing site, module and cell tech
    :type tests: sequence
    :return: spectral shift and filtered spectral shift
    """
    spectral_shift = dict.fromkeys(zip(*tests)[0])
    spectral_shift_filter = dict.fromkeys(zip(*tests)[0])
    for test in tests:
        site, pvmod, celltech = test
        spectral_shift[site]= correct_spectrum(site, pvmod, celltech)
        # filters
        filter = (
            (spectral_shift[site]['kt'] >= 0.7) &
            (spectral_shift[site]['kt'] < 1.0) &
            (spectral_shift[site]['ipoa'] >= 200) &
            (spectral_shift[site]['aoi']<=40)
        )
        spectral_shift_filter[site] = spectral_shift[site].dropna().loc[filter]
    return spectral_shift_filter, spectral_shift


def twoparameter(m):
    """
    Fit the First Solar 2-parameter model to the measured spectral shift, m.
    
    The First Solar formula uses only water vapor (pwat) and absolute airmass
    (amp).
    
    .. math::
    
       m_{fs} = a_0 + a_1 * amp + a_2 * pwat \
          + a_3 * \\sqrt \\right( amp \\left) \
          + a_4 * \\sqrt \\right( pwat \\left) \
          + a_5 * \\right(\\frac{amp}{pwat}\\left)
    
    :param m: spectral shift
    :returns: input matrix, coefficients, fitted data and residuals
    """
    b = m['m'].values.reshape(-1,1)
    n = b.size
    a = np.concatenate([
        np.ones((n, 1)), m[['amp', 'pwat']].values,
        np.sqrt(m[['amp', 'pwat']]).values,
        (m['amp'] / m['pwat']).values.reshape(-1,1)
    ], 1)
    x, res, rank, sv = lstsq(a, b)
    y = np.dot(a, x)
    return a, x, y, res


def threeparameter(m):
    """
    Extend the First Solar 2-parameter model to include AOD at 500[nm].
    
    .. math::
    
       m_{fs} = a_0 + a_1 * amp + a_2 * pwat  + a_3 * \\tau_{500[nm]} \
          + a_4 * \\sqrt \\right( amp \\left) \
          + a_5 * \\sqrt \\right( pwat \\left) \
          + a_6 * \\sqrt \\right( \\tau_{500[nm]} \\left) \
          + a_7 * \\right(\\frac{amp}{pwat}\\left) \
          + a_8 * \\right(\\frac{amp}{\\tau_{500[nm]}}\\left)
    
    :param m: spectral shift
    :returns: input matrix, coefficients, fitted data and residuals
    """
    b = m['m'].values.reshape(-1,1)
    n = b.size
    a = np.concatenate([
        np.ones((n, 1)), m[['amp', 'pwat', 'tau500']].values,
        np.sqrt(m[['amp', 'pwat', 'tau500']]).values,
        (m['amp'] / m['pwat']).values.reshape(-1,1),
        (m['amp'] / m['tau500']).values.reshape(-1,1)
    ], 1)
    x, res, rank, sv = lstsq(a, b)
    y = np.dot(a, x)
    return a, x, y, res


def make_plots(m):
    """
    Plot comparisons of measured versus Sandia 5th order airmass polynomial,
    spectral shift calculated from EQE and SPECTRL2 predicted spectra, First
    Solar 2-parameter model and FS model extended to include AOD at 500[nm].
    """
    f = plt.figure('fit')
    # calculated
    plt.subplot(2, 2, 1)
    plt.scatter(x=m['f1_meas'], y=m['f1_calc'], s=m['tau500'] * 100, c=m['pwat'])
    slope, intercept, rval, pval, stderr = linregress(m['f1_meas'], m['f1_calc'])
    x = np.linspace(0.9, 1.2, 10)
    plt.plot(x, x * slope + intercept, 'b',
             x, x * slope + intercept - 2 * stderr, 'r--',
             x, x * slope + intercept + 2 * stderr, 'r--')
    plt.xlim([0.95, 1.05])
    plt.ylim([0.95, 1.05])
    plt.title('measured vs Sandia\nr-val = %g' % rval)
    plt.xlabel('measured')
    plt.ylabel('predicted')
    plt.grid()
    # measured
    plt.subplot(2, 2, 2)
    plt.scatter(x=m['f1_meas'], y=m['m'], s=m['tau500'] * 100, c=m['pwat'])
    slope, intercept, rval, pval, stderr = linregress(m['f1_meas'], m['m'])
    x = np.linspace(0.9, 1.2, 10)
    plt.plot(x, x * slope + intercept, 'b',
             x, x * slope + intercept - 2 * stderr, 'r--',
             x, x * slope + intercept + 2 * stderr, 'r--')
    plt.xlim([0.95, 1.05])
    plt.ylim([0.95, 1.05])
    plt.title('measured vs predicted\nr-val = %g' % rval)
    plt.xlabel('measured')
    plt.ylabel('predicted')
    plt.grid()
    # 2-parameter
    a2, x2, y2, res2 = twoparameter(m)
    plt.subplot(2, 2, 3)
    plt.scatter(x=m['f1_meas'], y=y2.reshape((-1,)), s=m['tau500'] * 100, c=m['pwat'])
    slope, intercept, rval, pval, stderr = linregress(m['f1_meas'], y2.reshape((-1,)))
    x = np.linspace(0.9, 1.2, 10)
    plt.plot(x, x * slope + intercept, 'b',
             x, x * slope + intercept - 2 * stderr, 'r--',
             x, x * slope + intercept + 2 * stderr, 'r--')
    plt.xlim([0.95, 1.05])
    plt.ylim([0.95, 1.05])
    plt.title('measured vs 2-parameter\nr-val = %g' % rval)
    plt.xlabel('measured')
    plt.ylabel('predicted')
    plt.grid()
    # 3-parameter
    a3, x3, y3, res3 = threeparameter(m)
    plt.subplot(2, 2, 4)
    plt.scatter(x=m['f1_meas'], y=y3.reshape((-1,)), s=m['tau500'] * 100, c=m['pwat'])
    slope, intercept, rval, pval, stderr = linregress(m['f1_meas'], y3.reshape((-1,)))
    x = np.linspace(0.9, 1.2, 10)
    plt.plot(x, x * slope + intercept, 'b',
             x, x * slope + intercept - 2 * stderr, 'r--',
             x, x * slope + intercept + 2 * stderr, 'r--')
    plt.xlim([0.95, 1.05])
    plt.ylim([0.95, 1.05])
    plt.title('measured vs 3-parameter\nr-val = %g' % rval)
    plt.xlabel('measured')
    plt.ylabel('predicted')
    plt.grid()
    return f, x2, res2, x3, res3


if __name__ == '__main__':
    m, mraw = analyze_spectral_shift()
    mall = pd.concat(m.values())
    f = make_plots(m=mall)
