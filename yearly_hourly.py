#! /usr/bin/env python

"""
Comparison of spectral mismatch from NREL hourly yearly TMY3 weather data using
EQE and SPECTRL2/SOLPOS vs GHI/DNI/DHI, fAM and fAOI
http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2005/data/tmy3/724945TYA.CSV
"""

# imports
import os
import numpy as np
from scipy.constants import e as q, k, c, h
from scipy.interpolate import interp1d
from openpyxl import load_workbook
from matplotlib import pyplot as plt
from solar_utils import solposAM, spectrl2
import pandas as pd
from datetime import datetime
import pytz
import logging
from tracker_utils import calculate_AOI

logging.basicConfig(level=logging.DEBUG)

# CONSTANTS
DIRNAME = os.path.dirname(__file__)
EQE_FILENAME = 'meas_GenC-XUS-CS-AR.xlsx'
EQE_FULLPATH = os.path.join(DIRNAME, EQE_FILENAME)
EQE_SHEET = 'Sheet1'
EQE_LAMBDA = 'G2:G2003'
EQE = 'H2:H2003'
EQE_SCALING_FACTOR = 1.0234 # set Ee = 1 with AM1.5
TMY_FILE = '724945TYA.CSV'

# SPECTRL2 CONSTANTS
UNITS = 1 # set spectrl2 units to [W/m^2]
ALPHA = -1  # 1.14  # angrstrom turbidity exponent
ASYMM = -1  # 0.65  # asymmetry factor
O3 = -1  # ozone conc. calculated
ATMOS_COND = lambda aod, h2o: [ALPHA, ASYMM, O3, aod, h2o]
ALBEDO = lambda alb: [0.3, 0.7, 0.8, 1.3, 2.5, 4.0] + ([alb] * 6)

# system constants
module_tilt = 10  # module tilt relative to tracker
azimuth_system = 180  # system orientation relative to north
system_tracker_axes = 1  # number of system tracker axes, EG: 1-axis

# SPR-E20-435 Paramters
Isc0 = 6.5579  # [A]
Voc0 = 86.626  # [V]
Imp0 = 6.1304  # [A]
Vmp0 = 72.3771  # [V]
alpha_Isc0 = 0.000395
alpha_Imp0 = -0.00023
beta_Voc0 = -0.248
beta_Vmp0 =-0.2584
nDF = 1.011
pfAM = [0.957, 0.0402, -0.008515, 0.0007141, -0.00002132]
pfAOI = [1.0002, -0.000213, 3.63416E-05, -0.000002175, 5.2796E-08, -4.4351E-10]
C0 = 1.0115
C1 = -0.0115
C2 = 0.218474
C3 = -7.224183
Ns = 128  # number of cells
ACELL = 153.33 # [cm^2]

# thermal model
Rtherm_nat = -3.56
Rtherm_force = -0.0750
deltaTmod = 3  # [degC]

# reference conditions
T0 = 25.  # [degC]
E0 = 1000.  # [W/m^2]

# thermal voltage
deltaTc = lambda T_degC: nDF * k * (T0 + T_degC) / q

# air mass function
fAM = lambda AM: np.polyval(fAM[::-1], AM)

# AOI function
fAOI = lambda AOI: np.polyal(fAOI[::-1], AOI)

# get weather file header info
with open('724945TYA.CSV','r') as f:
    header = f.readline().rstrip().split(',')
wban, site_name, state, timezone, latitude, longitude, elevation = header
wban = int(wban)
timezone = float(timezone)
latitude = float(latitude)
longitude = float(longitude)
elevation = float(elevation)
location = [latitude, longitude, timezone]
timezone = pytz.FixedOffset(timezone * 60.)

# read weather data, skip header row, parse 1st 2 columns into date & time but
# don't set index column
data = pd.read_csv(TMY_FILE, skiprows=1, parse_dates=[[0,1]])

# set index
start = datetime(2010, 1, 1, 0, 30, 0)  # start of date/time of data
end = datetime(2010, 12, 31, 23, 30, 0)  # end of date/time of data
data.index = pd.date_range(start, end, freq='H', tz=timezone)

# make 2-axis plot
axs = data[['AOD (unitless)','Pwat (cm)']].plot(secondary_y='Pwat (cm)', grid=True)
axs.figure.show()

# important weather timeseries
GHI = data['GHI (W/m^2)']
DNI = data['DNI (W/m^2)']
DHI = data['DHI (W/m^2)']
alb = data['Alb (unitless)']
AOD = data['AOD (unitless)']
Pwat = data['Pwat (cm)']
pressure = data['Pressure (mbar)']
Tdry = data['Dry-bulb (C)']
Uwind = data['Wspd (m/s)']

# load EQE
wb = load_workbook(EQE_FULLPATH)
ws = wb.get_sheet_by_name(EQE_SHEET)
dt = np.dtype([('lambda','float'), ('EQE','float')])
EQE_lambda = [[x[0].value for x in ws.iter_rows(EQE_LAMBDA)],
              [y[0].value for y in ws.iter_rows(EQE)]]
EQE = np.array(zip(*EQE_lambda), dt)
f2 = plt.figure(2)
plt.plot(EQE['lambda'], EQE['EQE'])

# EQE interpolation function
EQE_func = interp1d(EQE['lambda'], EQE['EQE'] * EQE_SCALING_FACTOR / 100.)

# loop over year
for x in data.index:
    weather = [pressure[x], Tdry[x]]
    atmos_cond = ATMOS_COND(AOD[x], Pwat[x])
    albedo = ALBEDO(alb[x])
    date_time = x.timetuple()[:6]
    logging.debug('date/time: %s', x)

    # get solar angles
    angles, airmass =  solposAM(location, date_time, weather)
    zenith, azimuth = angles
    logging.debug('zenith: %g, azimuth: %g', zenith, azimuth)
    logging.debug('airmass: %g', airmass[1])

    # skip night
    if zenith >= 90.0:
        continue

    tracker_rotation, orientation, aoi = calculate_AOI(
        angles, azimuth_system, module_tilt, system_tracker_axes)

    # get solar spectrum
    args = (UNITS, location, date_time, weather, orientation,
            atmos_cond, albedo)
    specdif, specdir, specetr, specglo, specx = spectrl2(*args)

    # compute cell and module backside temperature
    Tmod = Tdry[x] + GHI[x] * np.exp(Rtherm_nat + Rtherm_force * Uwind[x])
    Tcell = Tmod + GHI[x] / E0 * deltaTmod
    dTc = deltaTc(Tcell)

    # parse pressure adjusted airmass
    airmass, am = airmass

    # get current
    Jsc = np.trapz(specglo * EQE_func(np.array(specx) * 1.e3) * specx,
        x=specx) * q / h / c / 1.e6
    Isc = Jsc * ACELL / 100. / 100.
    Ee = Isc / Isc0 / (1 + alpha_Isc0 * (Tcell - T0))
    Imp = Imp0 * (C0 * Ee + C1 *Ee**2)
    Vmp = Vmp0 + C2 * Ns * dTc * np.log(Ee) + C3 * Ns * (dTc * np.log(Ee))**2
    Pmp = Imp * Vmp
