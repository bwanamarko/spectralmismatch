# -*- coding: utf-8 -*-
"""
Spectral shift calculations for modules in NREL Report 61610 by Bill Marion

Running this module from the command line creates several plots: comparison
of QE from different celltech, the standard ASTM-G173 am1.5 spectrum, a
comparison of SPECTRL2 adn SMARTS at am1.5, and 2-d slices of spectral shift
for different celltech versus water vapor or aerosol.

This module has several convenience functions for getting or manipulating data
into the desired format:

* ``get_qe_norm()`` - gets normalized QE for all 3 celltech
* ``get_am15()`` - gets ASTM-G173 standard am1.5 spectrum
* ``get_solpos()`` - get solar position given location, time and weather
* ``get_spectrum() - get solar spectrum given location, time, weather and more
* ``calc_spectral_shift()`` - calculate spectral shift from QE and spectrum
* ``spectral_shift_sensitivity()`` - use uncertainty wrapper to calculate
  sensitivity
* ``pwat_spectral_shift()`` and ``tau500_spectral_shift()`` - make 2-d slices
* ``full_factorial()`` - surface plots of full 3-d space

SunPower (c) 2016
"""

import os
from datetime import datetime
import logging
import numpy as np
#from scipy.constants import e as qE, k as kB, c, h
from scipy.interpolate import interp1d
import pandas as pd
from solar_utils import solposAM, spectrl2
from uncertainty_wrapper import unc_wrapper
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

logging.basicConfig()
plt.ion()  # turn on interactive plotting

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
DIRNAME = os.path.dirname(__file__)
QE_PATH = os.path.join(DIRNAME, 'Data For Validating Models')

# SPECTRL2 CONSTANTS for ASTM173G Reference AM1.5 spectrum
# ========================================================
UNITS = 1 # set spectrl2 units to [W/m^2]
LOCATION = [39.74, -105.18, -7] # [deg] latitude, longitude at NREL
DATETIME = datetime(2005, 3, 1, 11, 26, 44) # date and time
WEATHER = [1013.25, 25] # ambient pressure [mB] and temperature [C]
ORIENTATION = [37, 180] # [deg] panel tilt and aspect
# turbidity alpha exponent, asymmetry factor, ozone conc., AOD and water vapor
ATMOS_COND = [1.14, 0.65, 0.34, 0.084, 1.42]
ALBEDO = [0.3, 0.7, 0.8, 1.3, 2.5, 4.0] + ([0.2] * 6) # albedo vs wavelength
# http://rredc.nrel.gov/solar/spectra/am1.5/
AM15_FULLPATH = os.path.join(DIRNAME, 'ASTMG173.csv')  # ref AM 1.5 spectrum
GLO_AM15 = 'Global_tilt__Wm2nm1'
WVL_AM15 = 'Wvlgth_nm'

# NREL QE measurements
# ====================
SEP = '\t'  # delimiter
# from Pandas read_csv() documentation: "you might consider index_col=False to
# force pandas to _not_ use the first column as the index"
INDEX_COL=False  # force Pandas not to use the first column as index
# arguments to use to read csv files and column name to use as index
QE_CSV_ARGS = {
    'cSi': {
        'kw': {
            'filepath_or_buffer': os.path.join(
                QE_PATH, 'x-Si Manufacturer1 ModelA.txt'),
            'nrows': 44
        }, 'index': 'WL(nm)'
    },
    'mSi': {
        'kw': {
            'filepath_or_buffer': os.path.join(
                QE_PATH, 'mSi Manufacturer2 ModelC.txt'),
            'nrows': 50
        }, 'index': 'Wavelength'
    },
    'CdTe': {
        'kw': {
            'filepath_or_buffer': os.path.join(
                QE_PATH, 'CdTe Manufacturer3 Model D.txt'),
            'nrows': 61
        }, 'index': 'AVG WL'
    },
}


def get_qe_norm(plotme=True):
    """
    Get normalized QE as fraction from NREL CSV files.
    """
    if plotme:
        f = plt.figure('QE')
    else:
        f = None
    qe = dict.fromkeys(QE_CSV_ARGS)  # blank dictionary of module cell tech types
    # iterate over QE files and read them
    for k, v in QE_CSV_ARGS.iteritems():
        # read csv files
        qe[k] = pd.read_csv(sep=SEP, index_col=INDEX_COL, **v['kw'])
        qe[k].columns = [col.strip() for col in qe[k].columns]  # strip whitespace
        qe[k] = qe[k].set_index(v['index'])  # set indices
        # create new column with normalized QE as fraction
        qe[k]['QEnorm'] = qe[k].iloc[:,0] / qe[k].iloc[:,0].max()
        if plotme:
            f = plt.figure('QE')
            qe[k]['QEnorm'].plot()  # plot normalized
    if plotme:
        # add legend and grid
        plt.legend(qe.keys())
        plt.grid()
    return qe, f


def get_am15(plotme=True):
    """
    Get ASTM-G173 Reference AM 1.5 Spectrum.
    """
    kwargs = {'delimiter': ',', 'skip_header': 1, 'names': True}
    am15 = np.genfromtxt(AM15_FULLPATH, **kwargs)
    e_am15 =  np.trapz(am15[GLO_AM15], x=am15[WVL_AM15])
    LOGGER.debug('ASTM-G173: E = %g', e_am15)
    if plotme:
        f = plt.figure('AM15')
        plt.plot(am15[WVL_AM15], am15[GLO_AM15])
        plt.title('ASTM-G173 Reference AM 1.5 Spectrum')
        plt.xlabel('Wavelength, $\lambda \ [nm]$')
        plt.ylabel('Global Tilt, $I(\lambda) \ [W/m^2/nm]$')
        plt.grid()
    else:
        f = None
    return am15, e_am15, f


def get_solpos(dt=DATETIME, location=LOCATION, weather=WEATHER):
    """
    Get solar position.
    """
    datevec = dt.timetuple()[:6]  # convert datetime to date vector
    angles, airmass =  solposAM(location, datevec, weather)
    angles = {'zenith': angles[0], 'azimuth': angles[1]}
    airmass = {'am': airmass[0], 'amp': airmass[1]}
    LOGGER.debug('SOLPOS: airmass = %g, zenith = %g', airmass['am'],
                 angles['zenith'])
    return angles, airmass


def get_spectrum(dt=DATETIME, location=LOCATION, weather=WEATHER,
                 orientation=ORIENTATION, atmos_cond=ATMOS_COND,
                 albedo=ALBEDO, plotme=True):
    """
    Get solar spectrum.
    """
    datevec = dt.timetuple()[:6]  # convert datetime to date vector
    specdif, specdir, specetr, specglo, specx = spectrl2(
        UNITS, location, datevec, weather, orientation, atmos_cond, albedo
    )
    spec = np.array(
        zip(specdif, specdir, specetr, specglo, specx),
        dtype=np.dtype(
            [('specdif', float), ('specdir', float), ('specetr', float),
             ('specglo', float), ('specx', float)]
        )
    )
    if plotme:
        fig_label = None
        try:
            fig_label = plotme.get_label()
        except AttributeError:
            fig_label = 'spectrl2'
        f = plt.figure(fig_label)
        plt.plot(specx, zip(specdif, specdir, specglo))
        plt.title(
            'SPECTRL2 at (lat: %g, lon: %g) on %s' % (
                location[0], location[1],
                datetime.strftime(dt, '%Y/%d/%m-%H:%M:%S')
            )
        )
        plt.xlabel('Wavelength, $\lambda \ [\mu m]$')
        plt.ylabel('Solar Spectrum, $I(\lambda) \ [W/m^2/\mu m]$')
        plt.legend(('Diffuse Tilt', 'Direct Tilt', 'Global Tilt'))
        plt.grid()
    else:
        f = None
    e_spec =  np.trapz(specglo, x=specx)
    LOGGER.debug('SPECTRL2: E = %g', e_spec)
    return spec, e_spec, f


def calc_spectral_shift(qe, spec, am15, e_spec, e_am15):
    """
    Calculate spectral shift.

    .. math::
      M = \frac{
        \int{ E \left( \lambda \right) SR \left( \lambda \right) d \lambda }
      } {
        \int{ E \left( \lambda \right) d \lambda }
      } \frac{
        \int{E_{G173} \left( \lambda \right) d \lambda }
      } {
        \int{
          E_{G173} \left( \lambda \right) SR \left( \lambda \right) d \lambda
        }
      }

    The spectral response[#sr]_ can be related to the quantum efficiency[#qe]_.

    .. math::
      SR = \frac{q_e\lambda}{hc}QE

    .. [#sr] `Spectral Response <http://www.pveducation.org/pvcdrom/solar-cell-operation/spectral-response>`_
    .. [#qe] `Quantum Efficiency <http://www.pveducation.org/pvcdrom/solar-cell-operation/quantum-efficiency>`_
    """
    # interpolation function for quantum efficiency, use 0 for extrapolated
    qe_func = interp1d(
        qe.index, qe['QEnorm'], bounds_error=False, fill_value=0
    )
    # current density [A/m^2]
    jsc = np.trapz(
        spec['specglo'] * qe_func(spec['specx'] * 1.0e3) * spec['specx'],
        x=spec['specx']
    ) / 1.0e6  # * qE / h / c
    # current density [A/m^2] at AM 1.5
    jsc_am15 = np.trapz(
        am15[GLO_AM15] * qe_func(am15[WVL_AM15]) * am15[WVL_AM15],
        x=am15[WVL_AM15]
    ) / 1.0e9  # * qE / h / c
    return jsc / e_spec * e_am15 / jsc_am15


@unc_wrapper
def spectral_shift_sensitivity(x):
    latitude, pwat, tau500, alpha = x
    location = [latitude[0], -105.18, -7]
    atmos_cond = [alpha[0], 0.65, 0.34, tau500[0], pwat[0]]
    qe, _ = get_qe_norm()
    am15, e_am15, _ = get_am15()
    spec, e_spec, _ = get_spectrum(
        dt=DATETIME, location=location, weather=WEATHER,
        orientation=ORIENTATION, atmos_cond=atmos_cond, albedo=ALBEDO,
        plotme=False
    )
    return np.array([
        calc_spectral_shift(qe['cSi'], spec, am15, e_spec, e_am15),
        calc_spectral_shift(qe['mSi'], spec, am15, e_spec, e_am15),
        calc_spectral_shift(qe['CdTe'], spec, am15, e_spec, e_am15)
    ]).reshape((3,1))


def pwat_spectral_shift(plotme=True):
    qe, _ = get_qe_norm()
    am15, e_am15, _ = get_am15()
    m_pwat = np.zeros((10, 3))
    for n, pwat in enumerate(np.linspace(0.5, 5.0, 10)):
        atmos_cond = [1.14, 0.65, 0.34, 0.084, pwat]
        spec, e_spec, _ = get_spectrum(
            dt=DATETIME, location=LOCATION, weather=WEATHER,
            orientation=ORIENTATION, atmos_cond=atmos_cond, albedo=ALBEDO,
            plotme=False
        )
        m_pwat[n, 0] = calc_spectral_shift(
            qe['cSi'], spec, am15, e_spec, e_am15)
        m_pwat[n, 1] = calc_spectral_shift(
            qe['mSi'], spec, am15, e_spec, e_am15)
        m_pwat[n, 2] = calc_spectral_shift(
            qe['CdTe'], spec, am15, e_spec, e_am15)
    if plotme:
        f = plt.figure('pwat')
        plt.plot(np.linspace(0.5, 5, 10), m_pwat)
        plt.title('Spectral Shift vs. Water  Vapor')
        plt.xlabel('pwat [cm]')
        plt.ylabel('Spectral Shift, M')
        plt.legend(['cSi', 'mSi', 'CdTe'])
        plt.grid()
    else:
        f = None
    return m_pwat, f


def tau500_spectral_shift(plotme=True):
    qe, _ = get_qe_norm()
    am15, e_am15, _ = get_am15()
    m_tau500 = np.zeros((10, 3))
    for n, tau500 in enumerate(np.linspace(0.05, 0.5, 10)):
        atmos_cond = [1.14, 0.65, 0.34, tau500, 1.42]
        spec, e_spec, _ = get_spectrum(
            dt=DATETIME, location=LOCATION, weather=WEATHER,
            orientation=ORIENTATION, atmos_cond=atmos_cond, albedo=ALBEDO,
            plotme=False
        )
        m_tau500[n, 0] = calc_spectral_shift(
            qe['cSi'], spec, am15, e_spec, e_am15)
        m_tau500[n, 1] = calc_spectral_shift(
            qe['mSi'], spec, am15, e_spec, e_am15)
        m_tau500[n, 2] = calc_spectral_shift(
            qe['CdTe'], spec, am15, e_spec, e_am15)
    if plotme:
        f = plt.figure('tau500')
        plt.plot(np.linspace(0.05, 0.5, 10), m_tau500)
        plt.title('Spectral Shift vs. AOD 500 nm')
        plt.xlabel('AOD 500 nm')
        plt.ylabel('Spectral Shift, M')
        plt.legend(['cSi', 'mSi', 'CdTe'])
        plt.grid()
    else:
        f = None
    return m_tau500, f


# locations around reference that correspond to am = linspace(1.0, 5.0, 41)
AM_LOCS = np.array([[  -7.46780778,  -94.14144971],
       [  18.65983933, -115.21290785],
       [  33.00356917, -113.32730312],
       [  41.72101098, -110.36885761],
       [  47.09432124, -110.0396908 ],
       [  50.53655246, -112.38342178],
       [  52.18636212, -118.09627006],
       [  56.8082162 , -109.88411445],
       [  57.82429549, -115.49921909],
       [  60.57971619, -110.78379795],
       [  60.79956668, -117.73640138],
       [  62.47271979, -116.51099958],
       [  63.82750573, -115.83838738],
       [  64.14743768, -119.63370191],
       [  64.78889963, -121.22221776],
       [  65.34669184, -122.72590822],
       [  67.33465465, -116.90683091],
       [  66.81195427, -123.23629044],
       [  67.44502086, -123.48893806],
       [  68.6385692 , -120.73724875],
       [  69.37757783, -119.78139201]])


def full_factorial(celltech='cSi', saveme=True, plotme=False):
    """
    Map solution space M = f(AM, pwat, AOD, alpha).
    """
    qe, _ = get_qe_norm(plotme=False)
    am15, e_am15, _ = get_am15(plotme=False)
    nam, npwat, ntau, nalpha = 21, 26, 28, 20
    am = np.linspace(1.0, 5.0, nam)
    pwat = np.linspace(0.1, 5.1, npwat)
    tau500 = np.linspace(0.01, 0.55, ntau)
    alpha = np.linspace(0.1, 2.0, nalpha)
    mff = np.zeros((nam, npwat, ntau, nalpha))
    w, x, y, z = np.meshgrid(am, pwat, tau500, alpha, indexing='ij')
    for m in xrange(nam):
        for n in xrange(npwat):
            for o in xrange(ntau):
                for p in xrange(nalpha):
                    atmos_cond = [
                        z[m, n, o, p], 0.65, 0.34, y[m, n, o, p], x[m, n, o, p]
                    ]
                    loc = [AM_LOCS[m][0], AM_LOCS[m][1], LOCATION[2]]
                    spec, e_spec, _ = get_spectrum(
                        dt=DATETIME, location=loc, weather=WEATHER,
                        orientation=ORIENTATION, atmos_cond=atmos_cond,
                        albedo=ALBEDO, plotme=False
                    )
                    mff[m, n, o, p] = calc_spectral_shift(
                        qe[celltech], spec, am15, e_spec, e_am15
                    )
                    LOGGER.info(
                        'am: %g, pwat: %g, tau500: %g, alpha: %g, mff: %g',
                        am[m], pwat[n], tau500[o], alpha[p], mff[m, n, o, p]
                    )
    if saveme:
        np.savetxt(
            'm_cSi.csv',
            np.concatenate([c.reshape((-1, 1)) for c in (w, x, y, z, mff)], 1),
            delimiter=',', header='am,pwat,tau500,alpha,mff', comments=''
        )
    if plotme:
        m0, n0, o0, p0 = 2, 7, 4, 10  # indices of reference values
        # AMa, pwat surface
        ax = Axes3D(plt.figure())
        # switch x and y axes using transpose
        surf0 = ax.plot_surface(
            x[:, :, o0, p0].T, w[:, :, o0, p0].T, mff[:, :, o0, p0].T,
            rstride=1, cstride=1, cmap=cm.jet, linewidth=0.5, antialiased=False
        )
        ax.set_xlim(5,0)  # reverse axis
        ax.set_ylim(1,5)  # reverse axis
        ax.set_zlim(0.80, 1.1)
        ax.set_xlabel('water vapor [cm], $P_{wat}$')
        ax.set_ylabel('absolute airmass, $AM_a$')
        ax.set_zlabel('spectral shift, $M$')
        ax.set_title('%s Predicted Spectral Shift at AM1.5' % celltech)
        # AOD, alpha surface
        ax = Axes3D(plt.figure())
        surf1 = ax.plot_surface(
            y[m0, n0, :, :], z[m0, n0, :, :],
            (mff[m0, n0, :, :] + mff[m0 + 1, n0, :, :]) / 2.0,
            rstride=1, cstride=1, cmap=cm.jet, linewidth=0.5, antialiased=False
        )
        ax.set_zlim(0.98, 1.02)
        ax.set_title('%s Predicted Spectral Shift at AM1.5' % celltech)
        ax.set_xlabel('AOD (500nm), $\\tau$')
        ax.set_ylabel('Angstrom exponent, $\\alpha$')
        ax.set_zlabel('spectral shift, $M')
    return [am, pwat, tau500, alpha], [w, x, y, z], mff, [surf0, surf1]


if __name__ == "__main__":
    qe, _ = get_qe_norm()
    am15, e_am15, _ = get_am15()
    angles, airmass = get_solpos()
    spec, e_spec, _ = get_spectrum()
    correction = e_am15 / e_spec
    f = plt.figure('comparison')
    plt.plot(am15[WVL_AM15], am15[GLO_AM15],
             spec['specx'] * 1000.0, spec['specglo'] * correction / 1000.0)
    plt.title('SPECTRL2 vs SMARTS: Global Tilt')
    plt.xlabel('Wavelength, $\lambda \ [nm]$')
    plt.ylabel('Solar Spectrum, $I(\lambda) \ [W/m^2/nm]$')
    plt.legend(('SMARTS', 'SPECTRL2'))
    plt.grid()
    m = calc_spectral_shift(qe['cSi'], spec, am15, e_spec, e_am15)
    m_sens, cov, jac = spectral_shift_sensitivity(
        np.array([[39.74], [1.42], [0.084], [1.14]]), __covariance__=np.diag([0.0001] * 4)
    )
    m_pwat, _ = pwat_spectral_shift()
    m_tau500, _ = tau500_spectral_shift()
