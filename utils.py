# -*- coding: utf-8 -*-
"""
Utilities to read data from NREL model validation (Report 61610) by Bill Marion

SunPower (c) 2016
"""

import os
import csv
import logging
import numpy as np
from scipy.constants import e as qE, k as kB
import pandas as pd

logging.basicConfig()

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
DIRNAME = os.path.dirname(__file__)
FILE_PATH = os.path.join(
    DIRNAME, 'Data For Validating Models', 'Data For Validating Models'
)
SANDIA_MODEL_COEFFS = 'SandiaModelCoefficients.xlsx'
FILE_HEADER = [
    ("id", str),  # PV module identifier,
    ("city", str),  # City,
    ("state", str),  # State,
    ("tz", int),  # Time Zone,
    ("lat", float),  # Latitude degrees N+,
    ("lon", float),  # Longitude degrees W-,
    ("alt", float),  # Elevation meters above sea level,
    ("tilt", float),  # PV module tilt degrees,
    ("aspect", float),  # PV module azimuth degrees
]
COL_HEADERS = [
    ("timestamp", 'datetime64[s]'),  # Time Stamp (local standard time) yyyy-mm-ddThh:mm:ss,
    ("ipoa", float),  # POA irradiance CMP22 pyranometer (W/m2),
    ("ipoa_unc", float),  # POA irradiance uncertainty (%),
    ("mod_temp", float),  # PV module back surface temperature (degC),
    ("mod_temp_unc", float),  # PV module back surface temperature uncertainty (degC),
    ("isc", float),  # Isc (A),
    ("isc_unc", float),  # Isc uncertainty (%),
    ("pmp", float),  # Pmp (W),
    ("pmp_unc", float),  # Pmp uncertainty (%),
    ("imp", float),  # Imp (A),
    ("imp_unc", float),  # Imp uncertainty (%),
    ("vmp", float),  # Vmp (V),
    ("vmp_unc", float),  # Vmp uncertainty (%),
    ("voc", float),  # Voc (V),
    ("voc_unc", float),  # Voc uncertainty (%),
    ("ff", float),  # FF (%FF),
    ("ff_unc", float),  # FF uncertainty (%),
    ("delta_ipoa", float),  # Change in CMP22 during I-V scan +/- W/m2,
    ("delta_licor", float),  # Change in Li-COR during I-V scan +/- W/m2,
    ("mt5_temp", float),  # MT5 cabinet temperature (degC),
    ("dry_temp", float),  # Dry bulb temperature (degC),
    ("dry_temp_unc", float),  # Dry bulb temperature uncertainty (degC),
    ("rh", float),  # Relative humidity (%RH),
    ("rh_unc", float),  # Relative humidity uncertainty (%RH),
    ("patm", float),  # Atmospheric pressure (mb),
    ("patm_unc", float),  # Atmospheric pressure uncertainty (%),
    ("rain", float),  # Precipitation (mm) accumulated daily total,
    ("dni", float),  # Direct normal irradiance (W/m2),
    ("dni_unc", float),  # Direct normal irradiance uncertainty (%),
    ("dni_stdev", float),  # Direct normal irradiance standard deviation of 1-second samples of 5-second average (W/m2),
    ("ghi", float),  # Global horizontal irradiance (W/m2),
    ("ghi_unc", float),  # Global horizontal irradiance uncertainty (%),
    ("ghi_stdev", float),  # Global horizontal irradiance standard deviation of 1-second samples of 5-second average (W/m2),
    ("dhi", float),  # Diffuse horizontal irradiance (W/m2),
    ("dhi_unc", float),  # Diffuse horizontal irradiance uncertainty (%),
    ("dhi_stdev", float),  # Diffuse horizontal irradiance standard deviation of 1-second samples of 5-second average (W/m2),
    ("ghi_res", float),  # Solar QA residual (W/m2) = Direct*cos(zenith) + Diffuse Horiz. Global Horiz,
    ("soiling", float),  # PV module soiling derate,
    ("maint_start", str, 5),  # Daily maintenance start time (hour: minute) - no maintenance that day = 99:99,
    ("maint_stop", str, 5),  # Daily maintenance end time (hour: minute) - no maintenance that day = 99:99,
    ("maint_rain", float),  # Precipitation prior to daily maintenance (mm) accumulated daily total,
    ("npts", int),  # Number of I-V curve data pairs (n)
    ('IV', np.ndarray)
]


def get_nrel_data(filename=os.path.join('Cocoa', 'Cocoa_xSi12922.csv')):
    """
    get NREL data.
    """
    with open(os.path.join(FILE_PATH, filename), 'r') as fp:
        spamreader = csv.reader(fp)
        spamreader.next()
        header = {k: t(v) for (k, t), v in zip(FILE_HEADER, spamreader.next())}
        spamreader.next()
        data, npts = [], []
        for row in spamreader:
            N = int(row[41])
            npts.append(N)
            iv = np.array(zip(row[42:(42+N)], row[(42+N):(42+2*N)]),
                          dtype=np.dtype([('i', float), ('v', float)]))
            data.append(tuple(row[:42] + [iv]))
        data = np.array(data, dtype=np.dtype(COL_HEADERS))
    return header, data


def read_sandia_model_coeffs(workbook=SANDIA_MODEL_COEFFS):
    """
    Read the Sandia Array Performance Model coefficients from Excel workbook
    """
    return pd.read_excel(os.path.join(FILE_PATH, SANDIA_MODEL_COEFFS),
                         sheetname='Data', index_col='Model')


def calc_sandia(sapm, cell_temp, eff_irrad):
    """
    Calculate power with Sandia array performance model.
    """
    vt = sapm['n'] * kB * (cell_temp + 273.15) / qE # [V] thermal voltage
    imp = (
        sapm['Impo'] * (sapm['C0'] * eff_irrad + sapm['C1'] * eff_irrad ** 2.0)
        * (1.0 + sapm['aImp'] * (cell_temp - 25.0))
    )
    ns = sapm['Series Cells']
    vmp = (
        sapm['Vmpo'] + sapm['C2'] * ns * vt * np.log(eff_irrad) +
        sapm['C3'] * ns * (vt * np.log(eff_irrad)) ** 2.0 +
        sapm['BVmpo'] * (cell_temp - 25.0)
    )
    return imp * vmp, imp, vmp
    

if __name__ == '__main__':
     header, data = get_nrel_data()
     sapm = read_sandia_model_coeffs()
