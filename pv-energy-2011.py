"""
Creates an plot comparing relative error of predicted energy corrected for
spectral shift from the NREL mPERT datasets for four different module celltech
at Cocoa-FL. Two different spectral correction methods are shown: the Sandia
5th order airmass polynomial and the calculated spectral shift using EQE and
NREL SPECTRL2 predicted spectra. Yearly average errors for each prediction and
celltech are also calculated as well as their absolute differences, ie: Sandia
method versus SPECTRL2.
"""

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# get data
csi = pd.DataFrame.from_csv('cSi_filtered.csv')
cdte = pd.DataFrame.from_csv('CdTe_filtered.csv')
msi1 = pd.DataFrame.from_csv('mSi-1_filtered.csv')
msi2 = pd.DataFrame.from_csv('mSi-2_filtered.csv')

# get predicted yearly average dc energy error
csi_pmp_meas = csi.pmp_meas[csi.site == 'Cocoa'].resample('H').mean().sum()
cdte_pmp_meas = cdte.pmp_meas[cdte.site == 'Cocoa'].resample('H').mean().sum()
msi1_pmp_meas = msi1.pmp_meas[msi1.site == 'Cocoa'].resample('H').mean().sum()
msi2_pmp_meas = msi2.pmp_meas[msi2.site == 'Cocoa'].resample('H').mean().sum()
pv_energy = [[
        (1 - csi.pmp_calc[csi.site == 'Cocoa'].resample('H').mean().sum()/csi_pmp_meas),
        (1 - csi.pmp_sapm[csi.site == 'Cocoa'].resample('H').mean().sum()/csi_pmp_meas)
    ], [
        (1 - cdte.pmp_calc[cdte.site == 'Cocoa'].resample('H').mean().sum()/cdte_pmp_meas),
        (1 - cdte.pmp_sapm[cdte.site == 'Cocoa'].resample('H').mean().sum()/cdte_pmp_meas)
    ], [
        (1 - msi1.pmp_calc[msi1.site == 'Cocoa'].resample('H').mean().sum()/msi1_pmp_meas),
        (1 - msi1.pmp_sapm[msi1.site == 'Cocoa'].resample('H').mean().sum()/msi1_pmp_meas)
    ], [
        (1 - msi2.pmp_calc[msi2.site == 'Cocoa'].resample('H').mean().sum()/msi2_pmp_meas),
        (1 - msi2.pmp_sapm[msi2.site == 'Cocoa'].resample('H').mean().sum()/msi2_pmp_meas)
]]

# deltas between two prediction methods (sapm and qe/spectrl2)
deltas = np.diff(pv_energy)

if __name__ == '__main__':
    # make plots
    plt.ion()  # make plots interactive
    csi_pmp_meas = csi.pmp_meas[csi.site == 'Cocoa'].resample('H').mean().resample('M').sum()
    (1 - csi.pmp_calc[csi.site == 'Cocoa'].resample('H').mean().resample('M').sum()/csi_pmp_meas).plot(style='+-')
    (1 - csi.pmp_sapm[csi.site == 'Cocoa'].resample('H').mean().resample('M').sum()/csi_pmp_meas).plot(style='+--')
    cdte_pmp_meas = cdte.pmp_meas[cdte.site == 'Cocoa'].resample('H').mean().resample('M').sum()
    (1 - cdte.pmp_calc[cdte.site == 'Cocoa'].resample('H').mean().resample('M').sum()/cdte_pmp_meas).plot(style='o-')
    (1 - cdte.pmp_sapm[cdte.site == 'Cocoa'].resample('H').mean().resample('M').sum()/cdte_pmp_meas).plot(style='o--')
    msi1_pmp_meas = msi1.pmp_meas[msi1.site == 'Cocoa'].resample('H').mean().resample('M').sum()
    (1 - msi1.pmp_calc[msi1.site == 'Cocoa'].resample('H').mean().resample('M').sum()/msi1_pmp_meas).plot(style='x-')
    (1 - msi1.pmp_sapm[msi1.site == 'Cocoa'].resample('H').mean().resample('M').sum()/msi1_pmp_meas).plot(style='x--')
    msi2_pmp_meas = msi2.pmp_meas[msi2.site == 'Cocoa'].resample('H').mean().resample('M').sum()
    (1 - msi2.pmp_calc[msi2.site == 'Cocoa'].resample('H').mean().resample('M').sum()/msi2_pmp_meas).plot(style='v-')
    (1 - msi2.pmp_sapm[msi2.site == 'Cocoa'].resample('H').mean().resample('M').sum()/msi2_pmp_meas).plot(style='v--')
    
    plt.legend(['csi calc','csi sapm','cdte calc','cdte sapm','msi1 calc','msi1 sapm','msi2 calc','msi2 sapm'])
    plt.grid()
