#! /usr/bin/env python
"""
This is the original spectral mismatch investigation coauthored with Defne
`here <http://nbviewer.jupyter.org/gist/mikofski/54aac3ee295bab8118df>`_

The spectral shift is calculated for several climates and used to predict
power. The power is predicted with SAPM and compared to demonstrate that in a
hot dry climate, the instantaneous difference due to spectral mismatch could
be 2.5%.
"""

import os
import numpy as np
from scipy.constants import e as q, k, c, h
from scipy.interpolate import interp1d
from openpyxl import load_workbook
from matplotlib import pyplot as plt
from solar_utils import solposAM, spectrl2
from datetime import datetime
plt.ion()

# CONSTANTS
DIRNAME = os.path.dirname(__file__)  # path to present working directory

# SPECTRL2 CONSTANTS
UNITS = 1 # set spectrl2 units to [W/m^2]
LOCATION = [39.74, -105.18, -7] # [deg] latitude, longitude at NREL
DATETIME = datetime(2005, 3, 1, 11, 26, 44) # date and time
WEATHER = [1013.25, 25] # ambient pressure [mB] and temperature [C]
ORIENTATION = [37, 180] # [deg] panel tilt and aspect
# turbidity alpha exponent, asymmetry factor, ozone conc., AOD and water vapor
ATMOS_COND = [1.14, 0.65, 0.34, 0.084, 1.42]
ALBEDO = [0.3, 0.7, 0.8, 1.3, 2.5, 4.0] + ([0.2] * 6) # albedo vs wavelength

# SPR-E20-435 Paramters
ACELL = 153.33 # [cm^2] area of cell
Isc0 = 6.5579 # [A] short circuit current at STC
Voc0 = 86.626 # [V] open circuit voltage at STC
Imp0 = 6.1304 # [A] max power current at STC
Vmp0 = 72.3771 # [V] max power voltage at STC
# temperature coefficients
alpha_Isc0 = 0.000395 # [T^-1] short circuit current
alpha_Imp0 = -0.00023 # [T^-1] max power current
beta_Voc0 = -0.248 # [V/T] open circuit voltage
beta_Vmp0 =-0.2584 # [V/T] max power voltage
nDF = 1.011 # diode ideality factor
# airmass function
fAM = [0.957, 0.0402, -0.008515, 0.0007141, -0.00002132]
# angle of incidence function
fAOI = [1.0002, -0.000213, 3.63416E-05, -0.000002175, 5.2796E-08, -4.4351E-10]
# sandia model parameters
C0 = 1.0115
C1 = -0.0115
C2 = 0.218474
C3 = -7.224183
T0 = 25. # [C] ref temp
E0 = 1000. # [W/m^2] ref irradiance
deltaTc = nDF * k * (T0 + 273) / q # [V] thermal voltage
Ns = 128 # number of cells

# filename of ASTMG173 AM1.5 standard
AM15_FULLPATH = os.path.join(DIRNAME, 'ASTMG173.csv')

# get ASTMG173 AM1.5 standard
kwargs = {'delimiter': ',', 'skip_header': 1, 'names': True}
ASTMG173 = np.genfromtxt(AM15_FULLPATH, **kwargs)
E_AM15 =  np.trapz(ASTMG173['Global_tilt__Wm2nm1'], x=ASTMG173['Wvlgth_nm'])
f0 = plt.figure(0)
plt.plot(ASTMG173['Wvlgth_nm'], ASTMG173['Global_tilt__Wm2nm1'])
plt.title('ASTMG173 AM1.5')
plt.xlabel('Wavelength, $\lambda \ [nm]$')
plt.ylabel('Global Tilt, $I(\lambda) \ [W/m^2/nm]$')
plt.grid()

# filename of measured EQE
EQE_FULLPATH = os.path.join(DIRNAME, 'meas_GenC-XUS-CS-AR.xlsx')

# references of data in workbook
EQE_LAMBDA = 'G2:G2003' # lambda
EQE = 'H2:H2003' # measured EQE [%]

# get EQE
wb = load_workbook(EQE_FULLPATH)
ws = wb.get_sheet_by_name('Sheet1')
dt = np.dtype([('lambda','float'), ('EQE','float')])
EQE_lambda = [[x[0].value for x in ws.iter_rows(EQE_LAMBDA)],
              [y[0].value for y in ws.iter_rows(EQE)]]
EQE = np.array(zip(*EQE_lambda), dt)
f2 = plt.figure(2)
plt.plot(EQE['lambda'], EQE['EQE'])
plt.title('EQE GenC XUS AR')
plt.xlabel('Wavelength, $\lambda \ [nm]$')
plt.ylabel('Quantum Efficiency, $EQE(\lambda) \ [\%]$')
plt.xlim([250, 1250])
plt.grid()

# calculate Isc0
SCALING_FACTOR = 1.0234 # set Ee = 1 with AM1.5
EQE_func = interp1d(EQE['lambda'], EQE['EQE'] * SCALING_FACTOR / 100.)
# EQE_interp = np.interp(ASTMG173['Wvlgth_nm'], EQE['lambda'], EQE['EQE'])
JscAM15 = np.trapz(ASTMG173['Global_tilt__Wm2nm1'] *
               EQE_func(ASTMG173['Wvlgth_nm']) * ASTMG173['Wvlgth_nm'],
               x=ASTMG173['Wvlgth_nm']) * q / h / c / 1.e9
IscAM15 = JscAM15 * ACELL / 100. / 100.
EeAM15 = IscAM15/Isc0
EefAM = np.polyval(fAM[::-1],1.5)  * E_AM15 / E0

# get solar spectrum
args = (UNITS, LOCATION, DATETIME.timetuple()[:6], WEATHER, ORIENTATION,
        ATMOS_COND, ALBEDO)
specdif, specdir, specetr, specglo, specx = spectrl2(*args)
angles, airmass =  solposAM(LOCATION, DATETIME.timetuple()[:6], WEATHER)
f1 = plt.figure(1)
plt.plot(specx, zip(specdif, specdir, specglo))
plt.title(
    'SPECTRL2 at (lat: %g, lon: %g) on %s' % (
        LOCATION[0], LOCATION[1],
        datetime.strftime(DATETIME, '%Y/%d/%m-%H:%M:%S')
    )
)
plt.xlabel('Wavelength, $\lambda \ [\mu m]$')
plt.ylabel('Solar Spectrum, $I(\lambda) \ [W/m^2/\mu m]$')
plt.legend(('Diffuse Tilt', 'Direct Tilt', 'Global Tilt'))
plt.grid()

print 'zenith: %g, azimuth: %g' % (angles[0], angles[1])
print 'am: %g, amp: %g' % (airmass[0], airmass[1])

scaling = np.trapz(specglo, x=specx) / E0
Jsc = np.trapz(specglo / scaling * EQE_func(np.array(specx) * 1.e3) * specx,
    x=specx) * q / h / c / 1.e6
Isc = Jsc * ACELL / 100. / 100.
Ee = Isc/Isc0
Imp = Imp0 * (C0 * Ee + C1 *Ee**2)
Vmp = (Vmp0 + C2 * Ns * deltaTc * np.log(Ee) +
       C3 * Ns * (deltaTc * np.log(Ee))**2)
Pmp = Imp * Vmp

## Dry year
#args = (UNITS, LOCATION, DATETIME.timetuple()[:6], WEATHER, ORIENTATION,
#        [1.14, 0.65, 0.34, 0.55, 0.3], ALBEDO)
#specdif, specdir, specetr, specglo, specx = spectrl2(*args)
#angles, airmass =  solposAM(LOCATION, DATETIME.timetuple()[:6], WEATHER)
#f3 = plt.figure(3)
#plt.plot(specx, zip(specdif, specdir, specglo))
#plt.title('SPECTRL2: Dry - Boulder, CO, 2/27/2015 12:06 PM')
#plt.xlabel('Wavelength, $\lambda \ [\mu m]$')
#plt.ylabel('Solar Spectrum, $I(\lambda) \ [W/m^2/\mu m]$')
#plt.legend(('Diffuse Tilt', 'Direct Tilt', 'Global Tilt'))
#plt.grid()
#
#scaling = np.trapz(specglo, x=specx) / E0
#JscDry = np.trapz(specglo / scaling * EQE_func(np.array(specx) * 1.e3) * specx,
#    x=specx) * q / h / c / 1.e6
#IscDry = JscDry * ACELL / 100. / 100.
#EeDry = IscDry/Isc0
#ImpDry = Imp0 * (C0 * EeDry + C1 * EeDry**2)
#VmpDry = (Vmp0 + C2 * Ns * deltaTc * np.log(EeDry) +
#          C3 * Ns * (deltaTc * np.log(EeDry))**2)
#PmpDry = ImpDry * VmpDry
