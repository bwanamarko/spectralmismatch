from solar_utils import solposAM
from uncertainty_wrapper import unc_wrapper
import numpy as np

DT = [2005, 3, 1, 11, 26, 44]
WEATHER = [1013.25, 25]
LOC = [39.74, -105.18, -7]

fj = unc_wrapper(lambda x: np.array(solposAM([x[0][0], LOC[1], LOC[2]], DT, WEATHER)).reshape(-1, 1))
avg, cov, jac = fj(np.array([[LOC[0]]]), __covariance__=np.array([[ 0.0001]]))
print avg, cov, jac
print np.array(solposAM(LOC, DT, WEATHER)).reshape(-1, 1)
