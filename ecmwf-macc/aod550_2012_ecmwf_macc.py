#!/usr/bin/env python
from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()
server.retrieve({
    "class": "mc",
    "dataset": "macc",
    "date": "2012-01-01/to/2012-12-31",
    "expver": "rean",
    "grid": "0.75/0.75",
    "levtype": "sfc",
    "param": "207.210",
    "step": "3/6/9/12/15/18/21/24",
    "stream": "oper",
    "format": "netcdf",
    "time": "00:00:00",
    "type": "fc",
    "target": "aod550_2012_macc.nc",
})
