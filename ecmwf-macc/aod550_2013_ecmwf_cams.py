#!/usr/bin/env python
from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()
server.retrieve({
    "class": "mc",
    "dataset": "macc_nrealtime",
    "date": "2013-01-01/to/2013-12-31",
    "expver": "0001",
    "levtype": "sfc",
    "param": "207.210",
    "step": "0/3/6/9/12/15/18/21/24/27/30/33/36/39/42/45/48/51/54/57/60/63/66/69/72/75/78/81/84/87/90/93/96/99/102/105/108/111/114/117/120",
    "stream": "oper",
    "time": "00:00:00",
    "type": "fc",
    "target": "aod550_2013_cams.nc",
})