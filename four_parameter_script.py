from scipy.stats import linregress
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
m['Cocoa']['site'] = 'Cocoa'
m['Eugene']['site'] = 'Eugene'
m['Golden']['site'] = 'Golden'
mall = pd.concat(m.values())

def four_parameter(am, pwat, tau500, alpha, coeffs):
    return (
        coeffs[0] +
        coeffs[1] * am +
        coeffs[2] * pwat +
        coeffs[3] * tau500 +
        coeffs[4] * alpha +
        coeffs[5] * (am - 3.0) * (tau500 - 0.28) +
        coeffs[6] * (am - 3.0) * (alpha - 1.05) +
        coeffs[7] * (tau500 - 0.28) * (alpha - 1.05) +
        coeffs[8] * (am - 3.0) ** 2.0 +
        coeffs[9] * (pwat - 2.6) ** 2.0 +
        coeffs[10] * (alpha - 1.05) ** 2.0
    )
CSI_COEFFS = [
    1.04170816341411,
    -0.00840141510756903,
    0.0100789613458131,
    -0.101690006877521,
    -0.0144304470463807,
    -0.0655966359077981,
    -0.00835029592505709,
    -0.0662335019999851,
    -0.00487302551462757,
    -0.00365040610825545,
    0.019065088663993
]
CDTE_COEFFS = [
    1.08745257395684,
    -0.022617389207453,
    0.0194139548330263,
    -0.172887111226838,
    -0.0289518609321241,
    -0.0994358539470388,
    -0.0156408876907161,
    -0.124475414617483,
    -0.00618300121011323,
    -0.00563770218785326,
    0.0267493519749856,
]

def make_plots(m, mall, celltech, coeffs=None):
    # plot
    fcalc = plt.figure('calc')
    # calculated
    plt.scatter(x=m['Cocoa']['f1_meas'], y=m['Cocoa']['f1_calc'], c='b')
    plt.scatter(x=m['Eugene']['f1_meas'], y=m['Eugene']['f1_calc'], c='r')
    plt.scatter(x=m['Golden']['f1_meas'], y=m['Golden']['f1_calc'], c='g')
    slope, intercept, rval, pval, stderr = linregress(mall['f1_meas'], mall['f1_calc'])
    x = np.linspace(0.9, 1.2, 10)
    plt.plot(x, x * slope + intercept, 'b',
             x, x * slope + intercept - 2 * stderr, 'r--',
             x, x * slope + intercept + 2 * stderr, 'r--')
    plt.xlim([0.95, 1.05])
    plt.ylim([0.95, 1.05])
    plt.title('%s measured vs Sandia\nr-val = %g' % (celltech, rval))
    plt.xlabel('measured')
    plt.ylabel('predicted')
    plt.legend(['$\\mu$', '$\\mu-2\\sigma$', '$\\mu+2\\sigma$',
                'Cocoa', 'Eugene', 'Golden'], loc=0)
    plt.grid()
    # measured
    fpred = plt.figure('pred')
    plt.scatter(x=m['Cocoa']['f1_meas'], y=m['Cocoa']['m'], c='b')
    plt.scatter(x=m['Eugene']['f1_meas'], y=m['Eugene']['m'], c='r')
    plt.scatter(x=m['Golden']['f1_meas'], y=m['Golden']['m'], c='g')
    slope, intercept, rval, pval, stderr = linregress(mall['f1_meas'], mall['m'])
    x = np.linspace(0.9, 1.2, 10)
    plt.plot(x, x * slope + intercept, 'b',
             x, x * slope + intercept - 2 * stderr, 'r--',
             x, x * slope + intercept + 2 * stderr, 'r--')
    plt.xlim([0.95, 1.05])
    plt.ylim([0.95, 1.05])
    plt.title('%s measured vs SPECTRL2\nr-val = %g' % (celltech, rval))
    plt.xlabel('measured')
    plt.ylabel('predicted')
    plt.legend(['$\\mu$', '$\\mu-2\\sigma$', '$\\mu+2\\sigma$',
                'Cocoa', 'Eugene', 'Golden'], loc=0)
    plt.grid()
    return fcalc, fpred


    # # 2-parameter
    # m_2param = four_parameter(
        # am=m['amp'], pwat=m['pwat'], tau500=0.084, alpha=1.14, coeffs=coeffs
    # )
    # ax = plt.subplot(2, 2, 3)
    # sc = plt.scatter(x=m['f1_meas'], y=m_2param.reshape((-1,)), c=m['site'], s=5)
    # slope, intercept, rval, pval, stderr = linregress(m['f1_meas'], m_2param.reshape((-1,)))
    # x = np.linspace(0.9, 1.2, 10)
    # plt.plot(x, x * slope + intercept, 'b',
             # x, x * slope + intercept - 2 * stderr, 'r--',
             # x, x * slope + intercept + 2 * stderr, 'r--')
    # plt.xlim([0.95, 1.05])
    # plt.ylim([0.95, 1.05])
    # plt.title('measured vs 2-parameter\nr-val = %g' % rval)
    # plt.xlabel('measured')
    # plt.ylabel('predicted')
    # plt.grid()
    # cbar = plt.colorbar(sc, ticks=[1, 2, 3], orientation='horizontal')
    # cbar.ax.set_xticklabels(['Cocoa', 'Eugene', 'Golden'])  # horizontal colorbar
    # # 4-parameter
    # m_4param = four_parameter(
        # am=m['amp'], pwat=m['pwat'], tau500=m['tau500'], alpha=m['alpha'],
        # coeffs=coeffs
    # )
    # ax = plt.subplot(2, 2, 4)
    # sc = plt.scatter(x=m['f1_meas'], y=m_4param.reshape((-1,)), c=m['site'], s=5)
    # slope, intercept, rval, pval, stderr = linregress(m['f1_meas'], m_4param.reshape((-1,)))
    # x = np.linspace(0.9, 1.2, 10)
    # plt.plot(x, x * slope + intercept, 'b',
             # x, x * slope + intercept - 2 * stderr, 'r--',
             # x, x * slope + intercept + 2 * stderr, 'r--')
    # plt.xlim([0.95, 1.05])
    # plt.ylim([0.95, 1.05])
    # plt.title('measured vs 4-parameter\nr-val = %g' % rval)
    # plt.xlabel('measured')
    # plt.ylabel('predicted')
    # plt.grid()
    # cbar = plt.colorbar(sc, ticks=[1, 2, 3], orientation='horizontal')
    # cbar.ax.set_xticklabels(['Cocoa', 'Eugene', 'Golden'])  # horizontal colorbar
    # fix plot layout