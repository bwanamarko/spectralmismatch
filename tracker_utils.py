#! /usr/bin/env python

"""
Tracker utilities for calculating angle of incidence, orientation and tracker
rotation. Coordinate system
"""

import numpy as np
from past.builtins import basestring  # for python 2 to 3 compatibility
import logging

logging.basicConfig()

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
AXES = {'x': 0, 'y': 1, 'z': 2}


def rotation_matrix(angle, axis):
    """
    Return a rotation matrix that when multiplied by a column vector returns
    a new column vector that is rotated clockwise around the given axis by the
    given angle.

    :param angle: Angle of rotation (radians)
    :type angle: float
    :param axis: Axis of rotation
    :type axis: int, str
    :eturns: rotation matrix

    References:
       `Rotation Matrix <https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations>`_

    """
    r11 = r22 = np.cos(angle)
    r21 = np.sin(angle)
    r12 = -r21
    rot = np.matrix([[1, 0, 0],
                     [0, r11, r12],
                     [0, r21, r22]])
    if isinstance(axis, basestring):
        # if axis is a string, convert it to an integer
        axis = AXES[axis]
    while axis:
        axis -= 1
        rot = np.roll(np.roll(rot, 1, 1), 1, 0)
    return rot


def calculate_AOI(angles, azimuth_system, module_tilt, system_tracker_axes,
                  is_backtracking, ewgcr, max_rotation):
    """
    Calculate angle of incidence (AOI), module orientation and tracker rotation
    in global reference frame. AOI is the delta between the solar vector and
    a normal to the module surface. Orientation is the zenith and azimuth of a
    vector normal to the module in the global reference frame. Tracker rotation
    is only applicable for single-axis trackers and is the rotation of the
    tracker in the plane of the modules around the y-axis

    :param angles: Solar zenith and azimuth in degrees.
    :type angles: sequence
    :param azimuth_system: System azimuth in degrees.
    :type azimuth_system: float
    :param module_tilt: Module tilt in degrees.
    :type module_tilt:
    :param system_tracker_axes:
    :type system_tracker_axes:
    :return: tuple containing tracker rotation, module orientation and AOI
    """

    # convert to radians
    ze_rad, az_rad = np.deg2rad(angles)
    mod_tilt_rad = np.deg2rad(module_tilt)  # right handed (from y to z)
    az_sys_rad = np.deg2rad(azimuth_system)  # right handed (from x to y)

    # Calculate solar vector
    #
    #                    Sun
    #                     *         ^ z Up
    #                     :\        |
    #                     : \       |
    #                     :  \  ze  |  x_sun = sin(ze)*sin(az)
    #                     :   \     |
    #                     :    \    |  y_sun = sin(ze)*cos(az)
    #                     :     \   |
    #                     :      \  |  z_sun = cos(ze)
    #                   z_sun     \ |
    #                     :        \|
    # (180) South <--+----:-y_sun--.+-------------.+--> y North (0)
    #                 \   :      .*  \          .*
    #                  \  :  sin(ze)  \       az
    #                   \ :  .*      x_sun  .*
    #                    \:.*           \ .*
    #              (x, y) *<-----az------*
    #                                     \ x East (90)

    solar_vector = np.array([[np.sin(ze_rad) * np.sin(az_rad)],
                             [np.sin(ze_rad) * np.cos(az_rad)],
                             [np.cos(ze_rad)]])
    LOGGER.debug('solar vector\n\tx=%g,\ty=%g,\tz=%g',
                  solar_vector[0], solar_vector[1], solar_vector[2])

    # Calculate AOI using 3-D rotation matrices
    # https://en.wikipedia.org/wiki/Rotation_matrix#In_three_dimensions

    # Rotate solar vector around z-axis to remove system azimuth
    # NOTES:
    # 1. Azimuths are LH following the NREL convention, IE: 0 is north,
    #    90 is east, 180 is south and -90 is west.
    # 2. System azimuth convention follows NREL convention, IE: positive
    #    rotation is clockwise
    # 3. Positive system azimuth is equivalent to a negative rotation of the
    #    solar vector, but since LH is opposite of RH, it's also equivalent to
    #    positive RH rotation of the same value, so RH rotation matrix is used.
    # 4. South facing systems have system azimuth of 180, not zero.
    #
    #       Sun
    #        *   ^ z Up                   *   ^ z Up
    #        :\  |                        :\  |      system rotated relative
    #        : \ |                        : \ |     to global reference frame
    #        :  \|                        :  \|     ___---> y'
    #        :  .+------> y North (0)     :  .+---**-----------> y North (0)
    #        :.*/                         :.* |
    # (x, y) * /                 (x', y') *    |       system azimuth
    #         /x East (90)                      | x'   (-) <----> (+)

    R_z = rotation_matrix(az_sys_rad, 'z')
    # [[cos(az_sys_rad), -sin(az_sys_rad), 0],
    #  [sin(az_sys_rad),  cos(az_sys_rad), 0],
    #  [              0,                0, 1]])
    solar_vector_system = R_z * solar_vector  # solar vector in system coords
    LOGGER.debug('system solar vector\n\tx=%g,\ty=%g,\tz=%g',
                  solar_vector_system[0, 0], solar_vector_system[1, 0],
                  solar_vector_system[2, 0])

    # Rotate solar vector around x to remove system or module tilt
    # NOTES:
    # 1. Modules and systems facing south have system azimuth of 180 see above.
    # 2. Negative rotation of the modules or system is equivalent to positive
    #    rotation of solar vector so a RH rotation matrix is used.
    # 3. Residential modules in will typically have negative tilt in RH system,
    #    but by convention tilt is positive, so use LH system. Since rotation
    #    of solar vector is opposite of module tilt, in a RH system it's the
    #    same as module tilt in LH system, ie: positive.
    # 4. For tracker systems tilt is the angle of the surface relative to the
    #    the axis of tracker rotation. Use the same convention as for
    #    residential systems paying attention to the system zenith angle, ie:
    #    for zero system azimuth, trackers tilted north are positive and
    #    trackers tilted south are negative.

    R_x = rotation_matrix(mod_tilt_rad, 'x')
    # [[1,                 0,                  0],
    #  [0, cos(mod_tilt_rad), -sin(mod_tilt_rad)],
    #  [0, sin(mod_tilt_rad),  cos(mod_tilt_rad)]])
    # solar vector in module coordinates
    solar_vector_module = R_x * solar_vector_system
    LOGGER.debug('module solar vector\n\tx=%g,\ty=%g,\tz=%g',
                  solar_vector_module[0, 0], solar_vector_module[1, 0],
                  solar_vector_module[2, 0])

    # tracker rotation
    tracker_rotation = np.nan  # tracker rotation isn't relevant for fixed-tilt
    # switch calculation depending number of system tracker axes
    if system_tracker_axes == 2:
        # two-axis tracker
        # aoi is always zero and panel orientation is the same as solar vector
        # because sun vector is always normal to the panels
        aoi = 0
        orientation = [ze_rad, az_rad]
    else:
        # single-axis and fixed-tilt
        u_mod = np.array([[0],
                          [0],
                          [1]])  # unit vector normal to module
        if system_tracker_axes == 1:
            # single-axis tracker rotation is angle between z-axis and the
            # vector projected on the x-z plane of the module reference frame
            #
            #    Sun   ^ z Up
            #      \   |           rotate u_mod to sun vector projection
            # u_mod \  |           on x-z plane in module reference frame
            #        \ |    _-*'
            # x ______\|_-*'
            #       _-*'  module surface
            #   _-*'

            tracker_rotation = np.arctan2(solar_vector_module[0, 0],
                                          solar_vector_module[2, 0])
            # backtracking
            #
            #            *   ^ z Up   EWGCR = L / x
            #         Sun \  |        cos(theta) = L / Lx --> 0 as theta --> 90
            #              \ |        if cos(theta) < EWGCR, then Lx > x
            #    module     \|
            #             _-*'                       _-*'
            #       L _-*'   |\                L _-*'    \
            #     _-*'       | \             _-*'         \
            # _-*'  theta    |  \        _-*'              \
            # <---------x--------\------><-----------x-------------> next mod
            # <--------Lx-------->\

            lx = np.cos(tracker_rotation)  # always positive if |theta| < pi
            if is_backtracking and lx < ewgcr and solar_vector[2] > 0:
                backtracking = np.arccos(lx / ewgcr)  # correction
                # apply backtracking, change sign based on tracker rotation
                tracker_rotation -= backtracking * np.sign(tracker_rotation)
                LOGGER.debug('backtracking = ', backtracking * 180./np.pi)
            # max rotation
            max_rot_rad = np.deg2rad(max_rotation)
            tracker_rotation = np.amax([tracker_rotation, max_rot_rad[0]])
            tracker_rotation = np.amin([tracker_rotation, max_rot_rad[1]])
            R_y = rotation_matrix(tracker_rotation, 'y')
            # [[ cos(tracker_rotation), 0, sin(tracker_rotation)],
            #  [                     0, 1,                     0],
            #  [-sin(tracker_rotation), 0, cos(tracker_rotation)]])
            u_mod = R_y * u_mod  # apply tracker rotation
        else:
            # fixed tilt has no rotation
            tracker_rotation = 0
        # NOTE: R(-theta) = R.T ie: negative rotation is same as transpose
        #       because cos(-theta) = cos(theta) and sin(-theta) = -sin(theta)
        # unrotate normal vector to the global reference frame in reverse order
        u_mod = R_x.T * u_mod  # apply module tilt
        u_mod = R_z.T * u_mod  # apply system azimuth
        # calculate module orientation
        # * calculate magnitude of vector projected on x-y plane of global
        #   reference frame
        # * orientation zenith angle is angle between z-axis and module normal
        #   vector in the global reference frame
        # * orientation azimuth is angle between the y-axis and the projection
        #   of the unit vector on the x-y plane of the global reference frame
        u_mod_xy = np.sqrt(u_mod[0, 0] ** 2 + u_mod[1, 0] ** 2)
        orientation = np.array([np.arctan2(u_mod_xy, u_mod[2, 0]),
                                np.arctan2(u_mod[0, 0], u_mod[1, 0])])
        # convert radians to degrees
        tracker_rotation = np.rad2deg(tracker_rotation)
        orientation = np.rad2deg(orientation)
        aoi = np.rad2deg(np.arccos(np.dot(u_mod.T, solar_vector)))
        LOGGER.debug('tracker rotation: %g', tracker_rotation)
    LOGGER.debug('angle of incindence: %g', aoi)
    LOGGER.debug('tilt: %g, aspect: %g', orientation[0], orientation[1])
    return tracker_rotation, orientation, aoi


class Tracker(object):
    """
    Tracker.
    """
    def __init__(self, azimuth_system, module_tilt, system_tracker_axes,
                 is_backtracking=None, ewgcr=None, max_rotation=None):
        self.azimuth_system = azimuth_system
        self.module_tilt = module_tilt
        self.system_tracker_axes = system_tracker_axes
        self.is_backtracking = is_backtracking
        self.ewgcr = ewgcr
        self.max_rotation = max_rotation
        self.aoi = None
        self.tracker_rotation = 0
        self.orientation = [0, 0]

    def rotate(self, angles):
        self.tracker_rotation, self.orientation, self. aoi = calculate_AOI(
            angles, self.azimuth_system, self.module_tilt,
            self.system_tracker_axes, self.is_backtracking, self.ewgcr,
            self.max_rotation
        )


class TwoAxisTracker(Tracker):
    """
    Two axis tracker.
    """
    def __init__(self, azimuth_system, module_tilt):
        system_tracker_axes = 2
        super(TwoAxisTracker, self).__init__(azimuth_system, module_tilt,
                                             system_tracker_axes)


class SingleAxisTracker(Tracker):
    """
    Single axis tracker.
    """
    def __init__(self, azimuth_system, module_tilt, is_backtracking, ewgcr,
                 max_rotation):
        system_tracker_axes = 1
        super(SingleAxisTracker, self).__init__(
            azimuth_system, module_tilt, system_tracker_axes, is_backtracking,
            ewgcr, max_rotation
        )


class FixedTilt(Tracker):
    """
    Fixed tilt.
    """
    def __init__(self, azimuth_system, module_tilt):
        system_tracker_axes = 0
        super(FixedTilt, self).__init__(azimuth_system, module_tilt,
                                        system_tracker_axes)


if __name__ == '__main__':
    test_tracker = SingleAxisTracker(0, 0, False, 0.45, [-45., 45.])

