from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

celltech = 'CdTe'
ax = Axes3D(plt.figure())
surf = ax.plot_surface(
	coords[1][:,:,4,10].T, coords[0][:,:,4,10].T, mff[:,:,4,10].T,
	rstride=1, cstride=1, cmap=cm.jet, linewidth=0.5, antialiased=False
)
ax.set_xlim(5,0)
ax.set_ylim(1,5)
ax.set_zlim(0.80, 1.1)
ax.set_xlabel('water vapor [cm], $P_{wat}$')
ax.set_ylabel('absolute airmass, $AM_a$')
ax.set_zlabel('spectral shift, $M$')
ax.set_title('%s Predicted Spectral Shift at AM1.5' % celltech)
ax = Axes3D(plt.figure())
surf = ax.plot_surface(
	coords[2][2,7,:,:], coords[3][2,7,:,:], (mff[2,7,:,:] + mff[3,7,:,:]) / 2.0,
	rstride=1, cstride=1, cmap=cm.jet, linewidth=0.5, antialiased=False
)
ax.set_zlim(0.98, 1.02)
ax.set_title('%s Predicted Spectral Shift at AM1.5' % celltech)
ax.set_xlabel('AOD (500nm), $\\tau$')
ax.set_ylabel('Angstrom exponent, $\\alpha$')
ax.set_zlabel('spectral shift, $M$')
