"""
Spectral Mismatch
=================
Scripts and analyses for 43rd IEEE PVSC paper on effects of spectral mismatch

(c) 2016 SunPower

Spectral Analysis
-----------------
This script analyzes the specified NREL mPERT data, calculates measured
spectral shift and compares it to four predictive models.

PV Energy 2011
--------------
Command line script to plot of error in predicted energy from two different
spectral correction methods at Cocoa with 4 different celltech.

Spectral Correction
-------------------
This is the workhorse. Contains the atmosphere class to read in AOD and water
vapor from ECMWF netCDF4 files, calculates measured and predicted spectral
shift and predicted power.

Spectral Mismatch
-----------------
The initial investigation into errors in power prediction based on spectral
mismatch.

Spectral Shift
--------------
Contains many methods to predict spectral shift given QE and spectrum. Also
has methods for geting spectrum using SPECTRL2, getting solar position,
getting ASTM-G173 standard airmass 1.5 getting normalized QE. Also makes
surface plots and 2-d slices of full spectral mismatch space on airmass, water
vapor and AOD.

Tracker Utils
-------------
This contains several trackers, fixed tilt, single-axis and 2-axis, for
calculating angle of incidence and tracker normal.

Utilities
---------
Functions to load NREL mPERT data and Sandia coefficients.
"""
